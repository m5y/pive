<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if (has_post_thumbnail()) : ?>
		<section class="entry" style="background-image: url(<?php echo esc_attr(wp_get_attachment_image_url(get_post_thumbnail_id(), 'post-thumbnail', false)) ?>)">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<header class="entry__header alignwide">
							<h1><?php the_title(); ?></h1>
						</header><!-- .entry-header -->
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<section class="entry__content">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php
					the_content(); ?>
				</div>
			</div>
		</div>
	</section>
</article>