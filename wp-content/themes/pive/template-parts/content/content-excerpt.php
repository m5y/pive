<?php

/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
?>

<a href="<?php echo get_the_permalink(); ?>" class="tile" data-aos="fade-in">
    <?php if (has_post_thumbnail()) : ?>
        <figure class="tile__img">
            <?php
            // Lazy-loading attributes should be skipped for thumbnails since they are immediately in the viewport.
            the_post_thumbnail('archive-post-thumbnail', array('loading' => false, 'alt' => get_the_title()));
            ?>
            <figcaption class="tile__content">
                <div class="tile__date"><?php echo get_the_date('d.m.Y'); ?></div>
                <?php the_title('<div class="tile__title">', '</div>'); ?>
                <div class="tile__excerpt"><?php the_excerpt(); ?></div>
            </figcaption>
        </figure>
    <?php endif; ?>
</a>