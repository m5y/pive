<?php

/**
 * The header.
 *
 * This is the template that displays all of the <head> section and everything up until main.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="UTF-8">
    <!-- Force IE to use the latest rendering engine available -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('name'); ?></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header class="header" id="js-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="navbar" role="navigation">
                        <a href="<?php bloginfo('url'); ?>" class="navbar__brand">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/pure_logo.svg" alt="Pure Interactive" class="navbar__brand-img">
                        </a>
                        <?php if (has_nav_menu('header-nav')) : ?>
                            <div class="nabar__menu">
                                <button class="burger d-block d-lg-none" id="js-mobile-menu-btn">
                                    <span class="burger__icon"></span>
                                </button>
                                <?php
                                // Primary navigation menu.
                                wp_nav_menu(array(
                                    'menu_class' => 'menu',
                                    'menu_id' => 'js-header-menu',
                                    'container' => false,
                                    'theme_location' => 'header-nav',
                                ));
                                ?>
                                <!-- .main-navigation -->
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main class="wrapper">