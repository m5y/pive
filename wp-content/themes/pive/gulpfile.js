const gulp = require("gulp");
const sass = require('gulp-sass')(require('sass'));
const rename = require("gulp-rename");
const autoprefixer = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");
const livereload = require('gulp-livereload');
const path = require('path');


gulp.task("css", function () {
  return gulp
    .src("assets/scss/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer("last 2 version", "safari 5", "ie 10", "ie 11"))
    .pipe(cleanCSS())
    .pipe(rename(function (path) {
      path.extname = ".min.css";
    }))
    .pipe(gulp.dest("dist"))
    .pipe(livereload());
});

gulp.task("watch", function () {
  livereload.listen({ basePath: 'dist' });
  return gulp.watch('assets/**/*.scss', gulp.series('css'));
});
