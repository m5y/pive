module.exports = {
  plugins: [
    [
      '@babel/plugin-transform-runtime',
      { corejs: { version: 3 }, useESModules: true },
    ],
  ],
  presets: [
    [
      '@babel/preset-env',
      {
        modules: false,
        useBuiltIns: 'usage',
        corejs: { version: 3 },
      },
    ],
  ],
};
