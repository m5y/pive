<?php

/**
 * Pive functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Pive
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define('THEME_VERSION', '1.0.0');
define('ADMIN_THEME_VERSION', '1.0.0');

/**
 * Enqueue scripts and styles.
 *
 */
add_action('wp_enqueue_scripts', function () {
    wp_register_style('main-css', get_template_directory_uri() . '/dist/main.min.css', [], THEME_VERSION);
    wp_enqueue_style('main-css');

    wp_register_script("main-js", get_template_directory_uri() . "/dist/app.min.js", ['jquery'], THEME_VERSION, true);
    wp_enqueue_script('main-js');
});

/**
 * Get all necessary theme files
 */
$theme_dir = get_template_directory();

require_once $theme_dir . '/inc/custom-filters.php';
require_once $theme_dir . '/inc/custom-post-type.php';
require_once $theme_dir . '/inc/navigation.php';
require_once $theme_dir . '/inc/theme-functions.php';
