<?php

/**
 * The template for displaying the footer
 *
 *Contains the opening of the .footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
</main><!-- #main -->
<footer class="footer">
    <div class="footer__notes">
        <b>@<?php echo date("Y"); ?> Pure Interactive.</b>
    </div>
</footer>
<?php wp_footer(); ?>
</body>

</html>