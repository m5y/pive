<?php get_header(); ?>
<section class="gallery">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="masonry">
                    <?php
                    // WP_Query arguments
                    $args = array(
                        'post_type' => array('gallery'),
                        'post_status' => array('publish'),
                        'nopaging' => true,
                        'order' => 'ASC',
                        'orderby' => 'menu_order',
                    );

                    // The Query
                    $gallery_query = new WP_Query($args);

                    // The Loop
                    if ($gallery_query->have_posts()) {
                        while ($gallery_query->have_posts()) {
                            $gallery_query->the_post();
                            get_template_part('/template-parts/content/content-excerpt');
                        }
                    } else {
                        // no posts found
                    }

                    // Restore original Post Data
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>