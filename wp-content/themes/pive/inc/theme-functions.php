<?php

add_action('init', function () {
    add_theme_support('post-thumbnails');
    add_image_size('archive-post-thumbnail', 540, 0, true);
});
