<?php

/**
 * Register custom post type - gallery
 */
function gallery_post_type()
{

	$labels = array(
		'name'                  => _x('Wypieki', 'Post Type General Name', 'text_domain'),
		'singular_name'         => _x('Wypiek', 'Post Type Singular Name', 'text_domain'),
		'menu_name'             => __('Wypieki', 'text_domain'),
		'name_admin_bar'        => __('Wypieki', 'text_domain'),
		'archives'              => __('Archiwum', 'text_domain'),
		'attributes'            => __('Atrybuty', 'text_domain'),
		'parent_item_colon'     => __('Rodzic:', 'text_domain'),
		'all_items'             => __('Wszystkie elementy', 'text_domain'),
		'add_new_item'          => __('Dodaj nowy element', 'text_domain'),
		'add_new'               => __('Dodaj nowy', 'text_domain'),
		'new_item'              => __('Nowy element', 'text_domain'),
		'edit_item'             => __('Edytuj element', 'text_domain'),
		'update_item'           => __('Zaktualizuj element', 'text_domain'),
		'view_item'             => __('Zobacz element', 'text_domain'),
		'view_items'            => __('Zobacz elementy', 'text_domain'),
		'search_items'          => __('Szukaj elementu', 'text_domain'),
		'not_found'             => __('Nie znaleziono', 'text_domain'),
		'not_found_in_trash'    => __('Nie znaleziono w koszu', 'text_domain'),
		'featured_image'        => __('Dołączony obrazek', 'text_domain'),
		'set_featured_image'    => __('Dołącz obrazek', 'text_domain'),
		'remove_featured_image' => __('Usuń dołączony obrazek', 'text_domain'),
		'use_featured_image'    => __('Użyj jako dołączony obrazek', 'text_domain'),
		'insert_into_item'      => __('Wstaw do elementu', 'text_domain'),
		'uploaded_to_this_item' => __('Przesłano do tego elementu', 'text_domain'),
		'items_list'            => __('Lista elementów', 'text_domain'),
		'items_list_navigation' => __('Nawigacja po liście elementów', 'text_domain'),
		'filter_items_list'     => __('Filtruj listę elementów', 'text_domain'),
	);
	$args = array(
		'label'                 => __('wypiek', 'text_domain'),
		'description'           => __('Post Type Description', 'text_domain'),
		'labels'                => $labels,
		'supports'              => array('title', 'editor', 'thumbnail', 'revisions', 'page-attributes'),
		'taxonomies'            => array('category', 'post_tag'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-buddicons-community',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type('gallery', $args);
}
add_action('init', 'gallery_post_type', 0);
