const header = document.querySelector('#js-header');
const burger = document.querySelector('#js-mobile-menu-btn');
const menu = document.querySelector('#js-header-menu');
const body = document.querySelector('body');

const toggleMobileMenu = () => {
  menu.classList.toggle('menu--visible');
  body.classList.toggle('active');
  header.classList.toggle('header--active');
  burger.classList.toggle('burger--active');
};

burger.addEventListener('click', (e) => {
  e.preventDefault();
  toggleMobileMenu();
});
