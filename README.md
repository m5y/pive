# README

## Development

### Prerequisites

In order to develop this project you'll need standard Wordpress environment:

- [PHP](https://www.php.net/downloads.php) version 7.4 or greater
- [MySQL](https://www.mysql.com/downloads/) version 5.7 or greater or [MariaDB](https://mariadb.org/) version 10.2 or greater

Static assets are bundled with [Node.js](https://nodejs.org/en/download/) based tooling.

### Wordpress installation

Wordpress core is not included in this repository.

1. [Download](https://pl.wordpress.org/download/) and install Wordpress in root directory of this repository (so that `/wp-content/themes/pive` and `/wp-content/plugins` ends up in right folders).
2. Unzip `uploads.zip` into `/wp-content/uploads` (sent via email)
3. Import Database from `dump.sql` (sent via email)

### Static assets

#### Prerequisites

1. Go to the theme directory (`/wp-content/themes/pive`)
2. Install dependencies `npm install`

#### Bundling CSS

CSS is transformed from SCSS and bundled with Gulp.

1. Run `npm run build:css` to bundle CSS
2. Run `npm run watch:css` to start watcher on `assets/scss/**/*.scss` files

#### Bundling JavaScript

JavaScript is bundled with Webpack.

1. Run `npm run build:js` to bundle js
2. Run `npm run watch:js` to start watcher on `assets/js/**/*.js` files
